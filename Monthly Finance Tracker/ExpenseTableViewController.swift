//
//  ExpenseTableViewController.swift
//  Monthly Finance Tracker
//
//  Created by Daniel Dicks on 11/24/16.
//  Copyright © 2016 Daniel Dicks. All rights reserved.
//

import UIKit
import os.log

class ExpenseTableViewController: UITableViewController {
    //MAAK: Properties
    var expenses = [Expense]() 
    var money: Money = Money(money: 0.0, orginalMoney: 0.0)
    
//    var money = 500.00
//    var orginalMoney = 500.00
    var topView: MainViewController!
    //MARK: Functions
    func loadSampleExpenses(){
        let expense1 = Expense(date: "11/10/16", expense: "Food", value: "100")!
        money.money = (money.money) - 100
        let expense2 = Expense(date: "11/22/16", expense: "Pizza", value: "20")!
        money.money = (money.money) - 20
        let expense3 = Expense(date: "11/5/16", expense: "Target", value: "40")!
        money.money = (money.money) - 40
        expenses.append(expense1)
        expenses.append(expense2)
        expenses.append(expense3)
    }
    func changeMoney(newMoney: Double){
        money.money = newMoney;
        money.orginalMoney = newMoney
        for exp in expenses{
            money.money -= Double(exp.value)!;
        }
        saveMoney()
    }
    func removeAll(){
        expenses.removeAll()
        tableView.reloadData()
        saveExpenses()
    }
    
    func addExpense(exp: Expense){
        let newIndexPath = IndexPath(row: expenses.count, section: 0)
        expenses.append(exp)
        money.money = (money.money) - Double(exp.value)!
        tableView.insertRows(at: [newIndexPath], with: .automatic)
        update()
    }
    func update(){
        topView.MoneyLeft.text = String(format: "%.2f", (money.money))
        saveExpenses()
        saveMoney()
    }
    func saveExpenses() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(expenses, toFile: Expense.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Expenses successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save expense...", log: OSLog.default, type: .error)
        }
    }
    func saveMoney(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(money, toFile: Money.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Money successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save Money...", log: OSLog.default, type: .error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //load sample data
//        loadSampleExpenses()
        // Load any saved expenses, otherwise load sample data.
        if let savedExp = loadExpenses() {
            expenses += savedExp
//            for exp in expenses {
//                money = money - Double(exp.value)!
//            }
        }
        if let savedMoney = loadMoney() {
            os_log("loaded money", log: OSLog.default, type: .debug)
            money = savedMoney
        }
        topView.MoneyLeft.text = String(format: "%.2f", (money.money))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "ExpenseTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ExpenseTableViewCell

        // Fetches the appropriate expense for the data source layout.
        let expense = expenses[indexPath.row]
        //change cell data
        cell.Date.text = expense.date
        cell.Expense.text = expense.expense
        cell.Value.text = expense.value
//        money = money - Int(expense.value)!
        //return cell
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            money.money = (money.money) + Double(expenses[indexPath.row].value)!
            expenses.remove(at: indexPath.row)
            saveExpenses()
            tableView.deleteRows(at: [indexPath], with: .fade)
            update()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
     
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowDetail" {
            let expenseDetailController = segue.destination as? AddViewController
            expenseDetailController?.saveButton.isEnabled = false
            let selectedExpenseCell = sender as? ExpenseTableViewCell
            
            let indexPath = tableView.indexPath(for: selectedExpenseCell!)
            
            let selectedExp = expenses[(indexPath?.row)!]
            expenseDetailController?.expense = selectedExp
            money.money = (money.money) + Double(selectedExp.value)!
        }
    }
    
    private func loadExpenses() -> [Expense]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Expense.ArchiveURL.path) as? [Expense]
    }
    private func loadMoney() -> Money? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Money.ArchiveURL.path) as? Money
        
    }

}
