//
//  ExpenseTableViewCell.swift
//  Monthly Finance Tracker
//
//  Created by Daniel Dicks on 11/24/16.
//  Copyright © 2016 Daniel Dicks. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var Expense: UILabel!
    @IBOutlet weak var Value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Date.layer.masksToBounds = true
        Date.layer.cornerRadius = 20
        Expense.layer.masksToBounds = true
        Expense.layer.cornerRadius = 20
        Value.layer.masksToBounds = true
        Value.layer.cornerRadius = 20
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
