//
//  MainViewController.swift
//  Monthly Finance Tracker
//
//  Created by Daniel Dicks on 12/17/16.
//  Copyright © 2016 Daniel Dicks. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    //MARK: Properties
    @IBOutlet weak var MoneyLeft: UILabel!
    var containerViewController: ExpenseTableViewController?
    
    
    //MARK: Actions
    @IBAction func unwindToMainView(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.source as? AddViewController, let expense = sourceViewController.expense {
            if let selectedIndexPath = containerViewController?.tableView.indexPathForSelectedRow {
                containerViewController?.expenses[selectedIndexPath.row] = expense
                containerViewController?.tableView.reloadRows(at: [selectedIndexPath], with: .none)
                containerViewController?.money.money = (containerViewController?.money.money)! - Double(expense.value)!
                containerViewController?.update()
                containerViewController?.saveExpenses()
            }else{
                // Add a new expense
                containerViewController?.addExpense(exp: expense)
            }
        }
        
    }
    
    @IBAction func changeMoney(_ sender: UIBarButtonItem) {
        
        // variable to store textfild in the alert
        var moneyTextField: UITextField?
        // create the alert
        let alert = UIAlertController(title: "Change Starting Money", message: String(format: "Enter new starting Money. Current starting money is %.2f", (containerViewController?.money.orginalMoney)!), preferredStyle: .alert)
        
        let changeAction = UIAlertAction(title: "Change", style: .default) {
            (alert: UIAlertAction!) -> Void in
            if let input = moneyTextField!.text {
                let newMoney = Double(input)!
                self.containerViewController?.changeMoney(newMoney: newMoney)
                self.containerViewController?.update()
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) -> Void in
            //Do nothing
        }
        alert.addAction(cancelAction)
        alert.addAction(changeAction)
        alert.addTextField { (textfield) -> Void in
            textfield.placeholder = "Enter new money here"
            textfield.keyboardType = .numberPad
            moneyTextField = textfield
        }
        present(alert, animated: true, completion:nil)
    }
    @IBAction func reset(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Clear Expenses", message: "Are you sure you want to clear the expenseses and reset the funds?", preferredStyle: .alert)
        let clearAction = UIAlertAction(title: "Clear", style: .default) { (alert: UIAlertAction!) -> Void in
            self.containerViewController?.removeAll()
            self.containerViewController?.money.money = (self.containerViewController?.money.orginalMoney)!
            self.containerViewController?.update()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (alert: UIAlertAction!) -> Void in
            //print("You pressed Cancel")
        }
        alert.addAction(cancelAction)
        alert.addAction(clearAction)
        present(alert, animated: true, completion:nil)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Use the edit button item provided by the table view controller.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tableSeg" {
            containerViewController = segue.destination as? ExpenseTableViewController
            containerViewController?.topView = self
        }
    }
}
