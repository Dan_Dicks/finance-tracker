//
//  Money.swift
//  Finance Tracker
//
//  Created by Daniel Dicks on 1/18/17.
//  Copyright © 2017 Daniel Dicks. All rights reserved.
//

import UIKit
import os.log

class Money: NSObject, NSCoding {
    //MARK: Types
    struct PropertyKey {
        static let money = "money"
        static let orginalMoney = "orginalMoney"
    }
    
    //MARK: Properties
    var money: Double
    var orginalMoney: Double
    
    init(money: Double, orginalMoney: Double) {
        self.money = money
        self.orginalMoney = orginalMoney
    }
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("money")
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.money, forKey: PropertyKey.money)
        aCoder.encode(self.orginalMoney, forKey: PropertyKey.orginalMoney)
    }
    required convenience init?(coder aDecoder: NSCoder) {
        let money = aDecoder.decodeDouble(forKey: PropertyKey.money)
        let orginalMoney = aDecoder.decodeDouble(forKey: PropertyKey.orginalMoney)
        self.init(money: money, orginalMoney: orginalMoney)
    }

}
