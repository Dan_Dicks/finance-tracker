//
//  Expense.swift
//  Monthly Finance Tracker
//
//  Created by Daniel Dicks on 11/24/16.
//  Copyright © 2016 Daniel Dicks. All rights reserved.
//

import UIKit
import os.log

class Expense: NSObject, NSCoding {
    //MARK: Types
    struct PropertyKey {
        static let date = "date"
        static let expense = "expense"
        static let value = "value"
    }

    //MARK: Properties
    var date: String
    var expense: String
    var value: String
    
    init?(date: String, expense: String, value: String){
        self.date = date
        self.expense = expense
        self.value = value
        
        if date.isEmpty || expense.isEmpty || value.isEmpty {
            return nil
        }
    }
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("expenses")
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(date, forKey: PropertyKey.date)
        aCoder.encode(expense, forKey: PropertyKey.expense)
        aCoder.encode(value, forKey: PropertyKey.value)
    }
    required convenience init?(coder aDecoder: NSCoder) {
        guard let expense = aDecoder.decodeObject(forKey: PropertyKey.expense) as? String else {
            os_log("Unable to decode the expense for a Expense object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let value = aDecoder.decodeObject(forKey: PropertyKey.value) as? String else {
            os_log("Unable to decode the value for a Expense object.", log: OSLog.default, type: .debug)
            return nil
        }
        let date = aDecoder.decodeObject(forKey: PropertyKey.date) as? String
        self.init(date: date!, expense:expense, value: value)
    }
}
