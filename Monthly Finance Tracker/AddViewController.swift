//
//  AddViewController.swift
//  Monthly Finance Tracker
//
//  Created by Daniel Dicks on 12/23/16.
//  Copyright © 2016 Daniel Dicks. All rights reserved.
//

import UIKit

class AddViewController: UIViewController, UITextFieldDelegate {
    //MARK: Variables
    var expense: Expense?
    var toolBar: UIToolbar = UIToolbar()

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var expenseTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var dateSelector: UIDatePicker!
    

    @IBOutlet weak var expenseLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    //MARK: Actions
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.isEnabled = false
        // Do any additional setup after loading the view.
        expenseTextField.delegate = self
        amountTextField.delegate = self
        
        expenseLabel.layer.masksToBounds = true
        amountLabel.layer.masksToBounds = true
        dateLabel.layer.masksToBounds = true
        expenseLabel.layer.cornerRadius = 20
        amountLabel.layer.cornerRadius = 20
        dateLabel.layer.cornerRadius = 20
        
        toolBar.barStyle = UIBarStyle.blackTranslucent
        toolBar.items = [UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddViewController.done))]
        toolBar.sizeToFit()
        amountTextField.inputAccessoryView = toolBar
        
        //if its being opended for showing detail/editing
        if(expense != nil){
            expenseTextField.text = expense?.expense
            amountTextField.text = expense?.value
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "M/d/y"
            let date = dateFormatter.date(from: (expense?.date)!)
            dateSelector.date = date!
        }
//        updateSaveButtonState()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func done(){
        amountTextField.resignFirstResponder()
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // Disable the Save button while editing.
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = expenseTextField.text
    }
    
    //MARK: Private Methods
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty.
        var text = expenseTextField.text ?? ""
        if !text.isEmpty{
            text = amountTextField.text ?? ""
        }
        saveButton.isEnabled = !text.isEmpty
    }
    
    
    // MARK: - Navigation
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentingInAddMealMode = presentingViewController is UINavigationController
        if isPresentingInAddMealMode{
            dismiss(animated: true, completion: nil)
        }else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated: true)
        }else {
            fatalError("The MealViewController is not inside a navigation controller.")
        }
        
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        // Configure the destination view controller only when the save button is pressed.
        if segue.identifier == "ShowDetail" {
            
        }else{
        
            guard let button = sender as? UIBarButtonItem, button === saveButton else {
                return
            }
            let exp = expenseTextField.text ?? "some expense"
            let value = amountTextField.text ?? "0"
        
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.short
        
            let date = dateFormatter.string(from: dateSelector.date)
            //set expense to given values
            expense = Expense(date: date, expense: exp, value: value)
        }
    }

}
