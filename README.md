# Finance Tracker
## Daniel Dicks

This repository contains the source code for my iOS finance tracker app.

I suggest using Xcode to run/simulate the app. In order to do so, download the "Monthly Finance Tracker" folder in the source, run Xcode and select that folder as the source to open an existing project.

![Photo Jan 10, 4 46 46 PM.png](https://bitbucket.org/repo/5pjjXo/images/3855710562-Photo%20Jan%2010,%204%2046%2046%20PM.png)![Photo Jan 10, 4 46 50 PM.png](https://bitbucket.org/repo/5pjjXo/images/159821333-Photo%20Jan%2010,%204%2046%2050%20PM.png)![Photo Jan 10, 4 50 35 PM.png](https://bitbucket.org/repo/5pjjXo/images/247014800-Photo%20Jan%2010,%204%2050%2035%20PM.png)